server {
    listen 9090;
    listen [::]:9090;

    server_name _ localhost;

    index index.html index.php index.php?$args;
    root  /var/www/wp;

    client_max_body_size 5M;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~* \.php$ {
        fastcgi_pass  php-fpm:9000;
        include       fastcgi_params;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param SCRIPT_NAME     $fastcgi_script_name;
    }

    location ~* /(?:upload|files)/.*\.php\$ {
        # Deny access to php files upload directory
        deny all;
    }

    location ~* \.(js|css|png|jpg|jpeg|gif|ico|svg)$ {
        expires max;
        log_not_found off;
    }

    # Block SQL injections
    # access_log /DATA/logs/nginx/blocked.log blocked;
    location ~* union.*select.*\( { deny all; }
    location ~* union.*all.*select.* { deny all; }
    location ~* concat.*\( { deny all; }
}

# Local Variables:
# mode: nginx
# End:
