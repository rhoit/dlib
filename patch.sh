#!/usr/bin/env bash

set -e

echo "* SubjectPlus patch"

PATH_sp="${1:-/var/www/html/sp/}"

if test -e "$PATH_sp"; then
    echo "  patching"
else
    echo "  '$PATH_sp' Not Found"
    exit 1
fi

sed -i "
    /lobjGuide = array()/a\$lobjGuide['id'] = \$lobjRow['subject_id'];
    " "$PATH_sp/lib/SubjectsPlus/API/GuidesWebService.php"

sed -i '
    /^$our_cats/s/,"Subject Librarians.*"//
    ' "$PATH_sp/subjects/staff.php"

sed -i '/$(document).ready/a \
    $(".pure-u-lg-2-3 > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)").html("<button onClick=window.history.back();>Go Back ←</button>")' \
    "$PATH_sp/subjects/databases.php"
